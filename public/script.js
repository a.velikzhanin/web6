function updatePrice() {
  
  let s = document.getElementsByName("prodType");
  let select = s[0];
  var price = 0;
  
  
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
  }
  
  // Скрываем или показываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = (select.value == "2" ? "block" : "none");
  
  // Смотрим какая товарная опция выбрана.
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });

  // Скрываем или показываем чекбоксы.
  let checkDiv = document.getElementById("checkboxes");

  if (select.value == "3") checkDiv.style.display ="block";
  else checkDiv.style.display="none";
  if (select.value == "1") 
  {
    checkDiv.style.display="none";
    radioDiv.style.display="none";
  }
  
  
  // Смотрим какие товарные свойства выбраны.
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
      }
    }
  });
  //месяцев
  var month = document.getElementsByName("month1");
  if (month[0].value.match(/^[0-9]+$/))
    
  {
    var prodPrice = document.getElementById("prodPrice");
  prodPrice.innerHTML = price*month[0].value + " рублей";
  }
    else 
    alert("Пожалуйста, вводите числа с целыми значениями");



  
}

function getPrices() {
  return {
    prodTypes: [75*1000, 0, 1000*1000],
    prodOptions: {
      option1: 150*1000,
      option2: 250*1000,
      option3: 500*1000,
    },
    prodProperties: {
      prop1: 100*1000,
      prop2: 100*1000,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {
  // Скрываем радиокнопки.
  let radioDiv = document.getElementById("radios");
  radioDiv.style.display = "none";
  let checkDiv = document.getElementById("checkboxes");
  checkDiv.style.display = "none";
  // Находим select по имени в DOM.
  let s = document.getElementsByName("prodType");
  let select = s[0];
  // Назначаем обработчик на изменение select.
  select.addEventListener("change", function (event) {
      updatePrice();
    });
  /*let s = document.getElementsByName("select11");
    let select = s[0];
    select.addEventListener("change", function () {
        update();
*/ 
  
  // Назначаем обработчик радиокнопок.  
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      updatePrice();
    });
  });

    // Назначаем обработчик радиокнопок.  
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      updatePrice();
    });
  });
// обработчик количества месяцев

let m = document.getElementsByName("month1");
let month = m[0];

month.addEventListener("change", function(event) {
  let target2 = event.target;
  console.log(target2.value);
  updatePrice();
});
});




/*
window.addEventListener('DOMContentLoaded', function (event) {
  let s = document.getElementsByName("myRadios1");
  s[0].addEventListener("click", function(event) {
    let select = event.target;
    let radios = document.getElementById("RadiosForComf");
    let checkbox = document.getElementById("CheckboxPremium");
    console.log(select.value);
    
    
    if (select.value == "1") {
      radios.style.display = "none";
      checkbox.style.display = "none";
    }
    else 
      if (select.value=="2")
      {
        radios.style.display = "block";
        checkbox.style.display = "none";

      }
      else 
        if (select.value=="3")
        {
          radios.style.display = "none";
          checkbox.style.display = "block";

        }
        else 
        {
          radios.style.display = "none";
          checkbox.style.display = "none";
        }
    
  });
  
  let r = document.querySelectorAll(".myRadios1 input[type=radio]");
  r.forEach(function(radio) {
    radio.addEventListener("click", function(event) {
      let r = event.target;
      console.log(r.value);
    });    
  });
  
});
window.addEventListener("DOMContentLoaded", function () {
  
  let b = document.getElementById("button1");
  b.addEventListener("click", clicked);
  let r = document.getElementById("result");
  r.innerHTML = "Результат будет тут";
});

function click2() {
  var choose = document.getElementsByName("radio-group3");
  var comf = document.getElementsByName("RadiosForComf");
  var prem = document.getElementsByName("CheckboxPremium");
  var month = document.getElementsByName("month");
  var r = document.getElementById("result");
  
  if (month[0].value.match(/^[0-9]+$/))
  {
    
  if (choose[0].checked)
  {
    r.innerHTML = 75000*month[0].value;
  }
  

  if (choose[0].checked)
  {
      if (comf[0].value=="100") r.innerHTML = 150000*month[0].value;
      if (comf[0].value=="150") r.innerHTML = 250000*month[0].value;
      if (comf[0].value=="250") r.innerHTML = 500000*month[0].value;
  }

  if (choose[0].checked)
  {
    if (prem[0].checked)
    r.innerHTML = 1100000*month[0].value;
    else r.innerHTML = 1000000*month[0].value;
  }
  
  
  }
  else 
    alert("Пожалуйста, вводите числа с целыми значениями");

  return false;
}
*/